import networkx as nx
from itertools import combinations
import pynauty as pn

# Example for (3,6,16)-graphs

# Initial parameters
d_initial = 2 # initial number of neighbors, see that the closest (3,5,n)-graph is n=13, then we need 2 neighbors to get n=16
value = 32 # maximum number of edges to get
dm = 1 #max(0, n - R(3,5) - 1)
k = 5 #because we are using all (3,5,n)-graphs

def edges_to_adj(edges):
    # Function to convert edges from networkx to adjacent list
    adj_list = {}
    for e in edges:
        if e[0] not in adj_list:
            adj_list[e[0]] = [e[1]]
        else:
            adj_list[e[0]].append(e[1])
        if e[1] not in adj_list:
            adj_list[e[1]] = [e[0]]
        else:
            adj_list[e[1]].append(e[0])
    return adj_list

class Graph():
    # Graph Class
    def __init__(self, E=[[1,2,3,4],[0,2],[0,1],[0],[0]]):
        self.E = E
        self.n = len(E)
        self.V = list(range(self.n))

    def __repr__(self):
        return self.E.__repr__()

    def copy(self):
        return Graph([N[:] for N in self.E])
    
    def add_node(self, neighbors = []):
        self.E.append(neighbors)
        self.V.append(self.n)
        for v in neighbors :
            self.E[v].append(self.n)
        self.n += 1
        
    def count_edges(self):
        count = 0
        for n in self.E:
            count += len(n)
        return count//2

    def is_ind_set(self, nodes):
        for pair in combinations(nodes, 2):
            if pair[0] in self.E[pair[1]] :
                return False
        return True

    def has_ind_set(self, k):
        for nodes in combinations(self.V, k): 
            if self.is_ind_set(nodes):
                return True
        return False
    
    def get_ind_sets(self, d, k):
        result = []
        for t in range(d, k):
            for nodes in combinations(self.V, t): 
                if self.is_ind_set(nodes):
                    result.append(nodes)
        return result
    
    def get_sub_graph(self, v):
        neighbors = self.E[v]
        labels = {neighbors[i]:i+1 for i in range(0, len(neighbors))}
        G = [[i for i in range(len(neighbors))]]
        for i in range(len(labels)):
            G.append([0])
        for i in neighbors:
            for j in self.E[i]:
                if j in neighbors:
                    G[labels[i]].append(labels[j])
        return G
    
    def has_node_deg(self, k):
        for n in self.E:
            if len(n) >= k:
                return True
        return False
    
    def remove_node(self, v):
        self.V.pop()
        self.n -= 1
        self.E.pop(v)
        self.E = [[w - int(w>v) for w in N if w != v] for N in self.E]
       
    def canonic(self):
        adj_list = {v:N for v,N in enumerate(self.E)}
        nauty = pn.Graph(self.n, directed=False, adjacency_dict=adj_list)
        label = pn.canon_label(nauty)
        dic = {v:k for k,v in enumerate(label)}
        g = Graph([sorted([dic[x] for x in self.E[label[v]]]) for v in self.V])
        return tuple([tuple(N) for N in g.E])    
   
        
def update_forbidden(G, k, ind_sets, fv):
    # Function that deletes independent sets that contain a vertex with degree
    # equal or bigger than k
    delete=[]
    
    if sum(fv) > 0:
        for n in range(len(G.E)):
            if len(G.E[n]) + fv[n] >= k:
                for m in range(len(ind_sets)):
                    if n in ind_sets[m]:
                        delete.append(m)
                    
    ind_sets = [x for y,x in enumerate(ind_sets) if y not in delete]
    
    return ind_sets

def update_ind_set(G, k, Sn, ind_sets):
    # Function that deletes independent sets if G /  the new independent
    # set  + one of the currently assigned induces a graph with a k-1 independent
    # set
    if len(Sn) > 0 and len(ind_sets) >0:
        for s in Sn:
            nodes = set([])
            H = G.copy()
            for j in s:
                nodes.add(j)
            for i in ind_sets[0]:
                nodes.add(i)
            nodes = list(nodes)
            nodes.sort(reverse=True)
            for n in nodes:
                H.remove_node(n)
            if H.has_ind_set(k-1) == True:
                return True
    return False
            
        
def construct(Gprime, k, d, e, ind_sets, fv, Sn = [], num_assigned =0, output =[]):
    
    if num_assigned == d:
        # If we already assigned all neighbors we just need to check if it is
        # a valid graph
        G = Gprime.copy()
        for n in Sn:
            G.add_node(list(n))
        G.add_node([x for x in range(G.n-d, G.n)])
        if G.has_ind_set(k+1) == False and G.count_edges() <= e:
            output.append(G.E)
            #print('Found graph:', G.E, '# edges:' , G.count_edges())
         
    ind_sets = update_forbidden(Gprime ,k, ind_sets, fv)
    
    while True:
        if update_ind_set(Gprime, k, Sn, ind_sets):
            ind_sets.pop(0)
        else:
            break
    
    if num_assigned < d and len(ind_sets) >= d - num_assigned:  
        
        i = len(Sn)
        t= len(ind_sets[0])
        n = 0
        for j in Sn:
            n += len(j)
        f = Gprime.count_edges() + d + n + t * (d-i)
        # Condition to break if with the current assignments we
        # will get more edges than needed

        if f <= e:
            Sn2 = Sn.copy()
            ind_sets2 = ind_sets.copy()
            Sn2.append(ind_sets[0])
            ind_sets2.pop(0)
            fv2 = fv.copy()
            for i in ind_sets[0]:
                fv2[i] += 1
                
            construct(Gprime, k, d, e, ind_sets, fv2, Sn2 , num_assigned +1 , output)
            construct(Gprime, k, d, e, ind_sets2, fv, Sn , num_assigned , output)
        
    return output

def neighboring_gluing_algorithm(Gprime, k, d, dm, e):
    if not Gprime.has_node_deg(d):
        print('no node of degree {} in G'.format(d))
    ind_sets = Gprime.get_ind_sets(dm,k)
    fv = [0] * Gprime.n
    output = construct(Gprime,k,d,e,ind_sets, fv)
    return output
   

# Example for all (3,6;16,<=32)-graphs

graph_list = ['r35_13.g6','r35_12.g6','r35_11.g6']
R = [[nx.read_graph6('r35_13.g6')], nx.read_graph6('r35_12.g6'),nx.read_graph6('r35_11.g6')]

i = 0

visited = set([])

for Gs in R:
    print(graph_list[i])
    j = 0
    for F in Gs:
        #if j % 100 == 0:
        #    print(j)
        adj_list = edges_to_adj(nx.edges(F))
        E = []
        sorted_list = {}
        for key in sorted(adj_list):
            sorted_list[key] = adj_list[key]
        for v, n in sorted_list.items():
            E.append(n)
        Gprime  =  Graph(E)

        d = d_initial + i       
        x = neighboring_gluing_algorithm(Gprime, k=k,d=d,dm=dm, e=value)

        for y in x:
            #print(x)
            H = Graph(y)
            canon = H.canonic()
            if canon not in visited:
                visited.add(canon)
        j += 1
        x = []
        print('visited:', len(visited))

    i += 1

print(visited) 
