import networkx as nx
import pynauty as pn

class Graph():
    def __init__(self, E=[[]]):
        self.E = E
        self.n = len(E)
        self.V = list(range(self.n))
  
    def copy(self):
        return Graph([N[:] for N in self.E])
    
    def add_node(self, node=[]):
        self.V.append(self.n)
        for n in node:
            self.E[n].append(self.n)
        self.E.append(node)
        self.n += 1
        
    def to_binary_edges(self):

        if self.n == 1:
            return ''

        num_edges = int(self.n * (self.n -1)/2)
        binary_edges = ''
        
        visited = []
        unique = set([])
        j = 1
        while True:
            if j in self.E[0]:
                binary_edges += '1'
            else:
                binary_edges += '0'
            visited.append(j)
            if len(visited) > 1:
                for i in range(len(visited)):
                    for k in range(i+1, len(visited)):
                        edge = '{}-{}'.format(i,k)
                        if edge not in unique:
                            if visited[k] in self.E[visited[i]]:
                                binary_edges += '1'
                            else:
                                binary_edges += '0'
                            unique.add(edge)

            j+=1
            if j == self.n:
                break
        return binary_edges

    def has_ind_set(self, k):
        node_list = [x for x in range(self.n)]
        for i in range(len(node_list)-1):
            t = 1
            nodes_left = node_list[i+t:] 
            visited = [node_list[i]]
            nodes = set(nodes_left)
            while len(nodes_left) > 0:
                n = nodes_left.pop(0)
                flag = any([n in self.E[x] for x in visited])
                if not flag:
                    visited.append(n)
                if len(visited) == k:
                    return True
                if len(visited) + len(nodes_left) < k:
                    if len(nodes) > 1 and (visited[-1] in nodes):
                        nodes.remove(visited[-1])
                        nodes_left = list(nodes)
                        visited = [node_list[i]]
                    else:
                        t += 1
                        if i + t  < len(node_list):
                            nodes_left = node_list[i+t:]
                            nodes = set(nodes_left) 
                            visited = [node_list[i]]
                        else:
                            break
        return False
    
    def remove_node(self, v):
        self.V.pop()
        self.n -= 1
        self.E.pop(v)
        self.E = [[w - int(w>v) for w in N if w != v] for N in self.E]
       
    def canonic(self):
        adj_list = {v:N for v,N in enumerate(self.E)}
        nauty = pn.Graph(self.n, directed=False, adjacency_dict=adj_list)
        label = pn.canon_label(nauty)
        dic = {v:k for k,v in enumerate(label)}
        g = Graph([sorted([dic[x] for x in self.E[label[v]]]) for v in self.V])
        return tuple([tuple(N) for N in g.E])   


def create_sequence(n):
    dic = [0]*n
    for i in range(1,n):
        dic[i] =  (i-1) + dic[i-1]
    return dic

def _check_triangle(edges, node, sequence, neighbors, visited):
    dif = len(edges) - sequence[node]
    if dif == 0:
        visited.add(edges+"0")
        _check_triangle(edges+"0", node, sequence,neighbors, visited)
        visited.add(edges+"1")
        _check_triangle(edges+"1", node, sequence,neighbors, visited)
    elif dif > 0 and dif <= node-1:
        flag = 0
        for j in range(dif):
            if edges[-dif+j] == '1' and edges[sequence[dif]+j] =='1':
                flag += 1         
            
        if flag > 0:
            if edges+"0" not in visited:
                visited.add(edges+"0")
                _check_triangle(edges+"0", node, sequence,neighbors, visited)
        else:
            if edges+"0" not in visited:
                visited.add(edges+"0")
                _check_triangle(edges+"0", node, sequence,neighbors, visited)
            if edges+"1" not in visited:  
                visited.add(edges+"1")
                _check_triangle(edges+"1", node, sequence,neighbors, visited)
                    
    if dif == node:
        neighbors.append(edges)

    return neighbors 

def check_triangle(edges, node):
    neighbors = []
    visited = set([])
    if node == 1:
        return [[],[0]]
    neighbors_binary = _check_triangle(edges, node, create_sequence(node+1), neighbors, visited)
    neighbors = []
    for n in neighbors_binary:
        neighbor = []
        combination = n[-node:]
        for c in range(node):
            if combination[c] == '1':
                neighbor.append(c)
        neighbors.append(neighbor)
    return neighbors


def _greedy_search(k, size, G , inspected , found ):

    canon = G.canonic()
    if canon not in inspected:
        inspected.add(canon)
        if G.n == size and not G.has_ind_set(k):
            found.append(G.E)
            return  inspected, found

    else: 
        return  inspected, found

    if (G.n <= size and G.has_ind_set(k)) or (max([len(N) for N in G.E]) >= k):
        return  inspected, found

    n = G.n
    neighbors = check_triangle(G.to_binary_edges(), n)
    for sets in neighbors:
        H = G.copy()
        H.add_node(sets)
        inspected, found = _greedy_search(k, size, H, inspected, found)

    return inspected, found

def greedy_search(k, size):
    G = Graph([[]])
    inspected = set([])
    found = []
    _, found = _greedy_search(k, size, G , inspected ,found)
    return found



found = greedy_search(5,13)
print(len(found))
print(found)
