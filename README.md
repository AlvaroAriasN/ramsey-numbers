# ramsey-numbers

This repo includes: 

- A greedy algorithm to compute ramsey graphs and ramsey numbers. 
- An adaptation of Neighboring Gluing algorithm to expand (3,k,n)-graphs into (3,k+1,n+d+1)-graphs
- All (3,5,n)-graphs needed for (3,6,n)-graphs from http://users.cecs.anu.edu.au/~bdm/data/ramsey.html


